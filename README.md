# Blast project workflow

######This repository hosts the code needed for the Blast project.

First fetch_sequences_skeleton.py can be executed to fetch fasta data for the given uniprot IDs.  
Next run_local_blast_skeleton.py can be executed to perfrom a BlastP or PSI-blast for evert ID.
Third step will be to run classify_go_skeleton.py to classify all protein pairs using GO-terms
Finally a ROC plot can be created using create_roc_plot_skeleton.py

For simplicity all data has been included.


```diff
Included Scripts
fetch_sequences_skeleton.py
run_local_blast_skeleton.py
classify_go_skeleton.py
create_roc_plot_skeleton.py

```